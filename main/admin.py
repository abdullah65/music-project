from django.contrib import admin
from .models import SignUp, Login, Music_Create, Music_List, Register

# Register your models here.
admin.site.register(SignUp)
admin.site.register(Login)
admin.site.register(Music_Create)
admin.site.register(Music_List)
admin.site.register(Register)