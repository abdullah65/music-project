from django.urls import path, include
from .views import index, main_view

urlpatterns = [
    path('', index.music_list, name='music_list'),
    path('homepage/', main_view.homePage, name='homepage'),
    path('signup/', main_view.signupPage, name='signup'),
    path('login/', main_view.loginPage, name='login'),
    path('logout/', main_view.logoutPage, name='logout'),
    path('musiccreate/', main_view.musicCreate, name='musiccreate'),
    path('musiclist/', main_view.musicList, name='musiclist'),
    path('usercreate/', main_view.UserCreatePage, name='usercreate'),
    path('userlist/', main_view.userList, name='userlist'),
    path('register/', main_view.registerPage, name='register'),
    path('deleteMusic/', main_view.deleteMusic, name='deleteMusic'),
     path('deleteUser/', main_view.deleteUser, name='deleteUser'),
]