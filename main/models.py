from django.db import models

# Create your models here.
class SignUp(models.Model):
    username = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    password = models.CharField(max_length=200, default='SOME STRING')
    #password2 = models.CharField(max_length=200, default='SOME STRING')

    def __str__(self):
        return str(self.id)


class Login(models.Model):
    username = models.CharField(max_length=200)
    password = models.CharField(max_length=200)

    def __str__(self):
        return str(self.id)

class Music_Create(models.Model):
    title = models.CharField(max_length=200)
    singer = models.CharField(max_length=200)
    composer = models.CharField(max_length=200)
    category = models.CharField(max_length=200)
    release_date = models.DateTimeField(auto_now_add=True)
    thumbnail = models.ImageField(upload_to="img1/%y")
    poster = models.ImageField(upload_to="img2/%y")

    def __str__(self):
        return str(self.id)

class Music_List(models.Model):
    title = models.CharField(max_length=200)
    singer = models.CharField(max_length=200)
    composer = models.CharField(max_length=200)
    category = models.CharField(max_length=200)
    release_date = models.DateTimeField(auto_now_add=True)
    thumbnail = models.ImageField(upload_to="img3/%y")
    poster = models.ImageField(upload_to="img4/%y")
    status = models.BooleanField(default=True)

    def __str__(self):
        return str(self.id)


















class Register(models.Model):
    username = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    password1 = models.CharField(max_length=200)
    password2 = models.CharField(max_length=200)

    def __str__(self):
        return str(self.id)