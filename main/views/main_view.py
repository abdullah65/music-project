from rest_framework.response import Response
from rest_framework.views import APIView
from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.parsers import MultiPartParser, FormParser
from django.conf import settings
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout
import re
from django.contrib.auth import get_user_model


from main.models import SignUp, Login, Music_Create, Music_List 
from main.form import SignUpForm, LoginForm, MusicCreateForm, MusicListForm, UserCreateForm


### signup

def signupPage(request):  
    message = ""
    if request.method == 'POST': 

        uname = request.POST.get('username')
        name = request.POST.get('name')
        email = request.POST.get('email')
        pass1 = request.POST.get('password1')
        pass2 = request.POST.get('password2')

        if pass1 != pass2:
            message = "Your password and confirm password are not same"
        else:
            my_user = User.objects.create_user(username=uname, email=email, password=pass1)
            my_user.save()
            return redirect("login")

    return render(request, "main/signup.html", {"message" : message})


### createuser

def UserCreatePage(request): 
    is_superuser = request.user.is_superuser
    is_authenticated = request.user.is_authenticated
    if not is_authenticated or not is_superuser:
         return redirect("userlist")

    message = "" 
    if request.method == 'POST': 

        uname = request.POST.get('username')
        name = request.POST.get('name')
        email = request.POST.get('email')
        pass1 = request.POST.get('password1')
        pass2 = request.POST.get('password2')

        if pass1 != pass2:
            message = "Your password and confirm password are not same"
        else:
            my_user = User.objects.create_user(username=uname, email=email, password=pass1)
            my_user.save()
            return redirect("/userlist")

    return render(request, "main/user_create.html", {"message" : message} )
        
### login

def loginPage(request): 
    message = ""

    if request.method == 'POST': 
        
        username = request.POST.get('username')
        password = request.POST.get('password')
        print("login unsername and password : ", username, password)

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("homepage")
        else:
            message = "Username or Password is incorrect!!"
 
    return render(request, "main/login.html", {"message":message})  


### homepage

def homePage(request):
    is_authenticated = request.user.is_authenticated
    if  not is_authenticated:
         return redirect("/login")
  
    return redirect("musiclist") 


### logout

def logoutPage(request):

    logout(request)
    return redirect('login')


### music create

def deleteUser(request):
    is_superuser = request.user.is_superuser
    is_authenticated = request.user.is_authenticated
    if not is_authenticated or not is_superuser:
         return redirect("userlist")
    if request.method == 'GET':
        music = User.objects.get(id=request.GET.get('id'))
        music.delete()
    
    return redirect("/userlist")
        

def deleteMusic(request):
    is_superuser = request.user.is_superuser
    is_authenticated = request.user.is_authenticated
    if not is_authenticated or not is_superuser:
         return redirect("musiclist")
    if request.method == 'GET':
        music = Music_List.objects.get(id=request.GET.get('id'))
        music.delete()
    
    return redirect("musiclist") 

def musicCreate(request):
    is_superuser = request.user.is_superuser
    is_authenticated = request.user.is_authenticated
    if  not is_authenticated:
         return redirect("/login")

    if request.method == 'POST':

        form = MusicListForm(data=request.POST,files=request.FILES)

        if form.is_valid():
            form.save()

        title = request.POST.get('title')
        singer = request.POST.get('singer')
        composer = request.POST.get('composer')
        category = request.POST.get('category')
        release_date = request.POST.get('release_date')
        thumbnail = request.POST.get('thumbnail')
        poster = request.POST.get('poster')

        return redirect("musiclist")

    else:
        form = MusicListForm()
    return render(request, "main/music_create.html",{"is_superuser" : is_superuser})




### music list 

def musicList(request):

    is_authenticated = request.user.is_authenticated
    if  not is_authenticated:
         return redirect("/login")

    obj = Music_List.objects.all()
    is_superuser = request.user.is_superuser

    


    return render(request, "main/music_list.html", {"obj": obj, "is_superuser" : is_superuser})

### uset list

def userList(request):
    is_authenticated = request.user.is_authenticated
    if  not is_authenticated:
         return redirect("/login")
    is_superuser = request.user.is_superuser

    all_users = User.objects.values()

    for user in all_users:
        print(user)


    return render(request, "main/user_list.html", {"users": all_users, "is_superuser" : is_superuser})



#user create


def UserCreate(request):
    is_authenticated = request.user.is_authenticated
    if  not is_authenticated:
         return redirect("/login")
    is_superuser = request.user.is_superuser

    if request.method == 'POST':
        form = UserCreateForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/userlist')
    else:
        form = UserCreateForm()
    return render(request, 'main/usercreate.html', {'form': form})



# def UserCreate(request):

#     is_authenticated = request.user.is_authenticated
#     if  not is_authenticated:
#          return redirect("/login")
#     is_superuser = request.user.is_superuser

#     if request.method == 'POST':
#         form = UserCreationForm(request.POST)
#         if form.is_valid():
#             form.save()
#             username = form.cleaned_data.get('username')
#             raw_password = form.cleaned_data.get('password1')
#             user = authenticate(username=username, password=raw_password)
#             login(request, user)
#             return redirect('/userlist')
#     else:
#         form = UserCreationForm()
#     return render(request, 'main/user_create.html', {'form': form})













def registerPage(request):  
    if request.method == 'POST': 
        print("post method") 
        form = CreateUserForm(request.POST)  
        if form.is_valid():  
            form.save() 
    else:
        form = CreateUserForm()
    context = {'form':form}    
    return render(request, "main/register.html", context)  