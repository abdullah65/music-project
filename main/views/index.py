from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render


def music_list(request):
    return render(request, "main/music_list.html")

def signup(request):
    return render(request, "main/signup.html")

def login(request):
    return render(request, "main/login.html")

def register(request):
    return render(request, "main/register.html")