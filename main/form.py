from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import SignUp, Login, Music_List, Music_Create

class UserCreateForm(UserCreationForm):
    username = forms.CharField(max_length=100, required=True, help_text='Username.')
    first_name = forms.CharField(max_length=100, required=True, help_text='Name.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'email', 'password1', 'password2', )

class SignUpForm(forms.ModelForm):
    class Meta:
        model = SignUp
        fields = '__all__'

class LoginForm(forms.ModelForm):
    class Meta:
        model = Login
        fields = '__all__'

class MusicCreateForm(forms.ModelForm):

    name = forms.TextInput()
    singer = forms.TextInput()
    composer = forms.TextInput()
    category = forms.TextInput()
    release_date = forms.DateTimeField()
    thumbnail = forms.ImageField()
    poster = forms.ImageField()
    status = forms.BooleanField()

    class Meta:
        model = Music_Create
        fields = '__all__'


class MusicListForm(forms.ModelForm):
    class Meta:
        model = Music_List
        fields = '__all__'